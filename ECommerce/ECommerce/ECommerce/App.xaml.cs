﻿using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using System.Globalization;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ECommerce
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer platformInitializer = null) : base(platformInitializer)
        {
            var culture = new CultureInfo("en-US");

            //Culture for any thread
            CultureInfo.DefaultThreadCurrentCulture = culture;

            //Culture for UI in any thread
            CultureInfo.DefaultThreadCurrentUICulture = culture;

            // AppResources.Culture = CrossMultilingual.Current.DeviceCultureInfo;

        }
        protected override  void OnInitialized()
        {
            InitializeComponent();
            VersionTracking.Track();

            MainPage = new MainPage();
        }
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            RegisterExternalService(containerRegistry);
            RegisterForNavigation(containerRegistry);
            RegisterService(containerRegistry);
            RegisterDialogs(containerRegistry);
        }
        private void RegisterForNavigation(IContainerRegistry containerRegistry)
        {
        }
        private void RegisterService(IContainerRegistry containerRegistry)
        {
        }

        private void RegisterDialogs(IContainerRegistry containerRegistry)
        {
        }

      
     
        private void RegisterExternalService(IContainerRegistry containerRegistry)
        {
        }
    }
    public  sealed partial class Routes
    {
        static readonly string navigation = nameof(NavigationPage);
    }
}
